build:
	nix build

build_static:
	nix build .#prd

dev:
	nix develop -c $$SHELL

run:
	dune build && dune exec quati_wiki

watch:
	dune build -w @run
