let () =
    let interface = "0.0.0.0" in
    let port = 8080 in
    Dream.run ~interface ~port
    @@ Dream.logger
    @@ Dream_livereload.inject_script ()
    @@ Dream.memory_sessions
    @@ Dream.router Router.routes
