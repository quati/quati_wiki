open Batteries
open Quati_wiki.Main

let host = "localhost";;
let port = 3001;;
let oauth_base = "/login/oauth";;
let client_id = "2178a1b6-b73f-4f1a-a251-3e9930b11b7d";;
let client_secret = "gto_puu4dgqz7hs6mlqi7s3zdqoqlbriz7o5x22y3q4se5h5s7zsnita";;
let redirect_uri = "http://localhost:8080/oauth";;
let state_length = 8;;

let get_verifier _ = "TESTE";;

let char_arr =
    let char_arr start finish =
        let start = Char.code start in
        let finish = Char.code finish in
        Array.init (finish - start) (fun i -> i + start) in
    let lowercase = char_arr 'a' 'z' in
    let uppercase = char_arr 'A' 'Z' in
    let digits = char_arr '0' '9' in
    Array.concat [ lowercase ; uppercase ; digits ];;

let get_authorization_code req =
    let state = 
    let rng = String.init 8 (fun _ -> 
        Array.length char_arr
        |> Random.int
        |> Array.get char_arr
        |> Char.chr) in
    let verifier = get_verifier rng in
    Printf.sprintf "%s.%s" rng verifier in
    let path = oauth_base ^ "/authorize" in
    let query =
        [ "client_id"       , [ client_id ]
        ; "redirect_uri"    , [ redirect_uri ]
        ; "response_type"   , [ "code" ]
        ; "state"           , [ state ]
        ] in
    let authorization_url = 
        Uri.make 
            ~scheme:"http" 
            ~host 
            ~port 
            ~path 
            ~query () 
        |> Uri.to_string in
    Dream.redirect req authorization_url;;

let handle_access_token_response body =
    let open Yojson in
    let body = Safe.from_string body in
    let get_yojson member mapper = body |> Safe.Util.member member |> mapper in
    let access_token = get_yojson "access_token" Safe.Util.to_string in 
    let refresh_token = get_yojson "refresh_token" Safe.Util.to_string in
    let expires_in = get_yojson "expires_in" Safe.Util.to_int in
    let expiration_time = (Unix.time () |> Int.of_float) + expires_in + 1 in
    (access_token, refresh_token, expiration_time);;

let get_access_token ~code ~state req =
    let (rng, verifier) = String.split state ~by:"." in
    if not @@ String.equal verifier @@ get_verifier rng
    then http_error "INVALID STATE"
    else
    let path = oauth_base ^ "/access_token" in
    let query = 
        [ "code"            , [ code ]
        ; "grant_type"      , [ "authorization_code" ]
        ; "client_id"       , [ client_id ]
        ; "client_secret"   , [ client_secret ]
        ] in
    let uri = Uri.make ~scheme:"http" ~host ~port ~path ~query () in
    let headers = Http.Header.of_list 
        [ "Content-Type", "application/x-www-form-urlencoded"] in
    let open Cohttp_lwt_unix in
    let open Lwt in
    let%lwt (access_token, refresh_token, expiration_time) = 
        Client.post ~headers uri >>= fun (_res, body) ->
        body |> Cohttp_lwt.Body.to_string >|= handle_access_token_response in
    let%lwt _ = Dream.set_session_field req "access_token" access_token in
    let%lwt _ = Dream.set_session_field req "refresh_token" refresh_token in
    let%lwt _ = Dream.set_session_field req "expiration_time" (string_of_int expiration_time) in
    Dream.redirect req "/p";;

let handler req =
    match Dream.query req "code", Dream.query req "state" with
    | Some code, Some state -> get_access_token ~code ~state req
    | _, _ -> get_authorization_code req;;
