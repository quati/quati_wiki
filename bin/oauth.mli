val oauth_handler: Dream.handler
(** [Oauth.oauth_handler] handles oauth user authentication and redirects the user
    to the correct page **)

val get_jwt: Dream.handler
