open Quati_wiki.Main

let get_projects req =
    let access_token = 
        match Dream.session_field req "access_token" with
        | Some at -> at
        | None -> "" in
    let open Dream_html in
    let open HTML in
    handle_htmx ~req ~title:"pages" 
    @@ nav [] [ txt "%s" access_token ];;
