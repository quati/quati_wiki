type routes =
      Root
    | Auth
    | Static
    | Project;;

let home = Dream.get "/" (fun req -> Dream.redirect req "/p");;

let static = 
    Dream.get "/static/**" 
        @@ Dream.static
            ~loader:(fun _root path _req ->
                match Static.read path with
                | None          -> Dream.empty `Not_Found
                | Some asset    -> Dream.respond asset)
            "";;

let oauth = Dream.scope "/oauth" [] [ Dream.get "" Oauth.oauth_handler ]


let routes =
    [ home
    ; static
    ; oauth
    ; Dream_livereload.route ()
    ];;
