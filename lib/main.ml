let handle_htmx ~req ~title body =
    let open Dream_html in
    match Dream.header req "Hx-Request" with
    | None      -> respond @@ Templates.page ~title body
    | Some _    -> respond body;;

let http_error ?(status=`Bad_Request) message =
    let open Dream_html in
    respond ~status @@ HTML.p [] [ txt "%s" message ];;
