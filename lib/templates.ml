open Dream_html
open HTML

let default_title = "quati.wiki";;

let page ~title:title' body' =
    let htmx_version = "1.9.11" in
    let htmx_integrity = "sha384-0gxUXCCR8yv9FM2b+U3FDbsKthCI66oH5IA9fHppQq9DDMHuMauqq1ZHBpJxQ0J0" in
    let stylesheet = "style.css" in
    html [ lang "en" ]
        [ head []
            [ title [] "%s - %s" default_title title' 
            ; meta [ charset "UTF-8" ]
            ; meta [ http_equiv `x_ua_compatible ; content "ID=edge" ]
            ; meta [ name "viewport" ; content "width=device-width, initial-scale=1.0" ]
            ; link [ rel "stylesheet" ; href "static/%s" stylesheet ]
            ; script    [ src "https://unpkg.com/htmx.org@%s" htmx_version
                        ; integrity "%s" htmx_integrity
                        ; crossorigin `anonymous 
                        ] 
                        ""
            ]
        ; body []
            [ header [] 
                [ a [ href "/" ; Hx.boost true ; Hx.target "main" ] 
                    [ txt "%s"  default_title 
                    ] 
                ]
            ; h1 [] [ txt  "%s" title' ] 
            ; main [] [ body' ]
            ]
        ];;

let p text = p [] [ txt "%s" text ];;

let article ~id:id' content =
    section [ id "article" ] 
        [ article [] [ txt ~raw:true "%s" content ]
        ; button [ Hx.get "/p/%s/edit" id' ; Hx.target "#article" ] [ txt "edit" ]
        ];;

let editor content = 
    form []
        [ textarea [] "%s" content
        ; button [ Hx.post "/" ] [ txt "send" ]
        ; button [ Hx.get "/" ] [ txt "cancel" ]
        ];;
